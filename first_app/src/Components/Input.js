import React from 'react';

function Input(props) {
  return (
    <div>
      <input name={props.name} value={props.name} placeholder="Enter Name" onChange={props.onInput}/>
      {props.name?<h1>Hello {props.name}</h1>:" Enter Name"}
    </div>
  );

  }
export default Input
