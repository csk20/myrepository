import React, { Component } from 'react';
import Input from './Input';

class MainApp extends Component {
  constructor(props) {
    super(props);
    this.state={
      name:''
    }
  }
  
  handleClick= e=>{
this.setState( {
name: e.target.value})};

  render() {
    return (
      <div>
        <Input  name={this.state.name} onInput={this.handleClick}/>
      </div>
    );
  }
}

export default MainApp;