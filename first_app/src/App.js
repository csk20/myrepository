import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import Counter from './Counter';
import Navbar from './Components/layout/Navbar'
import Login from './Components/auth/Login'
import Register from './Components/auth/Register'
import Footer from './Components/layout/Footer'
import Landing from './Components/layout/Landing'


import User from './User'
import MainApp from './Components/MainApp';

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
        <Route exact path="/" component={Landing} />
        <div className="container">

          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
        </div>


        <Footer />
      </div>
    </Router>
  );
}

export default App;
