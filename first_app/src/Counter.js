import React, { Component } from 'react';

class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    }
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(preState => {
      return { count: preState.count + 1}
     }) 
  }

  render() {
    return (
      <div>
        <h3>{this.state.count}</h3>
        <button onClick={this.handleClick}>Press to Increment</button>
      </div>
    );
  }
}

export default Counter;